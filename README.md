# fc-service
This repository is originally sourced from [here](https://gitlab.eclipse.org/eclipse/xfsc/cat/fc-service/-/tree/main) (as of 17.11.2023). Some configuration files have been modified for deployment on your own server.

# Installation
## Requirements
Ensure the following are available on your server:
- JDK 17
- Maven 3.5.4 (or newer)
- Git
- Docker

## Steps to build Federated Catalog (FC)
- Clone the repository onto your server
```
git clone https://gitlab.com/fdda1/fc-service.git
```
- Modify the config files: Current configurations are set for the server IP (129.26.160.213) on which FC is hosted. Adjust them based on your IP in the following files:
  - [.env](./docker/.env#L14)
  - docker-compose.yml - [119](./docker/docker-compose.yml#L119), [120](./docker/docker-compose.yml#L120), [128](./docker/docker-compose.yml#L128), [157](./docker/docker-compose.yml#L157) and [158](./docker/docker-compose.yml#L158)
  - application.yml - [26](./fc-demo-portal/src/main/resources/application.yml#L26) and [29](./fc-demo-portal/src/main/resources/application.yml#L29)
- Build jars (from the root of this repository).
```
mvn install -Dskiptests
```
- Run Docker (from the [Docker](./docker) folder)
```
docker compose --env-file dev.env up --build
``` 

## Keycloak HTTP Configuration Guide

This is the process for configuring a Keycloak server to allow HTTP access in environments where SSL is not yet set up. As a prerequisite the docker container with keycloak should be set up and operational (in otherwords, wait for a couple of minutes after running docker compose command used above)
- Open a new terminal session on the same server. In this terminal, access the command-line interface of a Docker container running Keycloak. This is done parallel to the other terminal where `docker compose up` is being executed.
```
sudo docker exec -it keycloak /bin/bash
```
- Inside the container, navigate to Keycloak's binary directory.
```
cd /opt/keycloak/bin
```
- Configure the Keycloak Administration CLI: Authenticate the CLI tool to the Keycloak server for administrative tasks. Replace &lt;server-ip&gt; with your server IP. The password is "admin".
```
./kcadm.sh config credentials --server http://<server-ip>:8080 --realm master --user admin
```
- Update the master realm's configuration to allow HTTP access
```
./kcadm.sh update realms/master -s sslRequired=NONE
```


## Keycloak setup
  - Once all components are started you need to setup Keycloak which is used as Identity and Access Management layer in the project. 
  - Open Keycloak admin console at `http://<server-ip>:8080/admin`, with "admin/admin" credentials, select `gaia-x` realm.
  - Go to `users` and create one to work with. Set its `username` and other attributes, save. Then go to `Credentials` tab, set its `password` twice, disable `Temporary switch`, save. 
  - Go to `Role Mapping` tab, in Client Roles drop-down box choose `federated-catalogue` client, select `Ro-MU-CA` role and add it to Assigned Roles.
  - Restart fc-server container to pick up changes applied above by running the following command in a different teminal session.
    ```
    docker restart fc-server
    ```

## API usage
- Download the API collection from [here](./fc-tools/Federated Catalogue API.postman_collection.json).
- Configure the following attributes in the collection file
  - [federated_catalogue_address](./fc-tools/Federated Catalogue API.postman_collection.json#L2324) : replace &lt;server-ip&gt; with your server IP 
  - [keycloak_user_username](./fc-tools/Federated Catalogue API.postman_collection.json#L2332) : `username` from Keycloak setup
  - [keycloak_user_password](./fc-tools/Federated Catalogue API.postman_collection.json#L2336) : `password` from Keycloak setup
  - [keycloak_address](./fc-tools/Federated Catalogue API.postman_collection.json#L2350) : replace &lt;server-ip&gt; with your server IP 
- Import the modified collection file into Postman.
- Authorization: Before sending a request to the FC, execute the `Authorization/ POST Retrieve access token` API to retrieve an access token and refresh token from Keycloak. These tokens are automatically set in the collection variables and used as a Bearer token for all requests within the collection.
- You can now test with APIs, such as `Users/ GET users` which returns your information including firstName, lastName, roleIds etc.
